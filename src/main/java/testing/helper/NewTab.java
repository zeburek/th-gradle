package testing.helper;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Root
 */
public class NewTab {

    // Tabs
    private JTabbedPane tab;
    private String newTabName = "New Tab";

// Creates a new tabbed pane with a text area
    protected JTabbedPane createTab(JScrollPane scroll) {
        tab = new JTabbedPane(JTabbedPane.TOP);
        tab.addTab(newTabName, scroll);

        return tab;
    }

}