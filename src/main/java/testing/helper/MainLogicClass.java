package testing.helper;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.io.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.AttributeSet;
import java.text.ParseException;
import java.util.TimerTask;
import java.util.Timer;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Root
 */
public class MainLogicClass extends JFrame {
    public static void main(String[] args) throws IOException, InterruptedException, ParseException {
        System.out.println("Hello, BugZilla!");
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(MainLogicClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        Main mainWindow = new Main();
        mainWindow.pack();
        mainWindow.setSize(1170, 600);
        mainWindow.setVisible(true);
       }
    //local variables
    public Process pr = null;
    public Process pr2 = null;
    public Process prDev = null;
    public Process prPack = null;
    public Process video = null;
    public String version;
    public String selectedDevice;
    public String globalFilter;
    public StringBuffer mainOutputBufferString = new StringBuffer("");
    public boolean globalFilterEnabled = false;
    public boolean haveEverStartedTimer = false;
    public Graphics2D g;
    public Timer bufferMainTimer = new Timer();
    public TimerTask bufferMainTimerTask;
    public long currentTimeStamp;
    public static AttributeSet mySet;
    public static JPanel jPanel2;
    int[] countTabs;
    public Color backgroundColorMainPane;
    public Color foregroundColorMainPane;
    public Font fontFamilyMainPane;
    public Font defaultFontFamilyMainPane;
    public int fontSizeMainPane;
    
    //Ending the selectors
    // Variables declaration - do not modify
    //Strings for Runtime.
    //Must be changed carefully!
    public String videoName;

    public void runs(String[] str, boolean FiltEn, String filter, JTextPane tp, JCheckBox c) throws IOException, InterruptedException {
        ProcessBuilder ps = new ProcessBuilder(str);
        ps.redirectErrorStream(true);
        clearSelect(c, tp);
        cancelLogcat(pr);
        //Timer Block begin
        if(haveEverStartedTimer){bufferMainTimerTask.cancel();}else{haveEverStartedTimer=true;}
        startOutputToPaneTimer(tp);
        //Timer Block ends
        pr = ps.start();
        new Thread() {
            @Override
            public void run() {
                BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                String line;
                try {
                    while ((line = in.readLine()) != null) {
                        if (FiltEn == false) {
                            if(!line.equals(""))
                            {
                                //ta.append(line + "\n");
                                //System.out.println("Check line property: \"" + line + "\"");
                                mainOutputBufferString.append(line + "\n");
                            }
                        } else {
                            if (line.toLowerCase().contains(filter.toLowerCase()) == true) {
                                //ta.append(line + "\n");
                                mainOutputBufferString.append(line + "\n");
                            }
                        }
                    }
                    //pr.waitFor();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("ok!");
            }
        }.start();
        
    }
    
    public void runs2(String[] str, JProgressBar ta) throws IOException, InterruptedException {
        ProcessBuilder ps = new ProcessBuilder(str);
        ps.redirectErrorStream(true);
        cancelLogcat(pr2);
        pr2 = ps.start();
        new Thread() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i <= 30; i++) {
                        ta.setValue(i);
                        Thread.sleep(100);
                        ta.setString("Копируем...");
                    }
                    BufferedReader in = new BufferedReader(new InputStreamReader(pr2.getInputStream()));
                    String line;
                    while ((line = in.readLine()) != null) {
                        if (line.toLowerCase().contains("pkg")){
                            for (int i = 31; i <= 85; i++) {
                                ta.setValue(i);
                                Thread.sleep(100);
                                ta.setString("Установка...");
                            }
                        }
                        if (line.toLowerCase().contains("success")){
                            for (int i = 86; i <= 100; i++) {
                                ta.setValue(i);
                                Thread.sleep(30);
                                ta.setString("Завершено!");
                            }
                        }
                        if (line.toLowerCase().contains("failure") || line.toLowerCase().contains("error")){
                            ta.setValue(100);
                            JOptionPane.showMessageDialog(null, "<html><table width='500px'>Кажется произошла ошибка при установке пакета:<br>"+line+"</table></html>", "Ошибка!", JOptionPane.CLOSED_OPTION);
                            ta.setString("Ошибка!");
                        }
                    }
                    System.out.println("ok!");
                } catch (IOException | InterruptedException ex) {
                    Logger.getLogger(MainLogicClass.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
    }
    
    public void runsSilence(String[] str) throws IOException, InterruptedException {
        ProcessBuilder ps = new ProcessBuilder(str);
        ps.redirectErrorStream(true);
        cancelLogcat(pr2);
        pr2 = ps.start();
    }
    
    static void saveToFile(String text) {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files (*.txt, *.log)","txt","log");
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(filter);
        if ( fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION ) {
            try ( FileWriter fw = new FileWriter(fc.getSelectedFile()) ) {
                fw.write(text);
            }
            catch ( IOException e ) {
                System.out.println("Всё погибло!");
            }
        }
    }
    public static void delete(String nameFile) throws FileNotFoundException {
        new File(nameFile).delete();
    }
    
    static void setExperimentsToFile(String text){
        System.out.println(System.getProperty("user.dir"));
        try ( FileWriter fw = new FileWriter(System.getProperty("user.dir")+"/yandex-browser-command-line") ) {
            fw.write(text);
        }
        catch ( IOException e ) {
            System.out.println("Всё погибло!");
        }
    }
    public void videoRecord(String[] str, JProgressBar ta) throws IOException, InterruptedException {
        videoRecord(str,ta,1);
    }

    public void videoRecord(String[] str, JProgressBar ta, int step) throws IOException, InterruptedException {
        ProcessBuilder vr = new ProcessBuilder(str);
        vr.redirectErrorStream(true);
        video = vr.start();
        new Thread() {
            @Override
            public void run() {
                BufferedReader on = new BufferedReader(new InputStreamReader(video.getInputStream()));
                String line;
                try {
                    while ((line = on.readLine()) != null) {
                        if (step==1){ta.setIndeterminate(true);}
                        else if (step==2){
                            ta.setIndeterminate(false);
                            ta.setString("Копирование ...");
                            for (int i = 0; i <= 100; i = i+2) {
                                ta.setValue(i);
                                Thread.sleep(100);
                            }
                        }
                    }
                    video.waitFor();
                } catch (IOException | InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("ok!");
                ta.setValue(100);
            }
        }.start();
    }

    public void cancelLogcat(Process pr) {
        if (pr != null) {
            pr.destroyForcibly();
            pr = null;
        }
    }

    public void clearSelect(JCheckBox c, JTextArea ta) {
        if (c.isSelected()) {
            ta.setText("");
        }
    }
    
    public void clearSelect(JCheckBox c, JTextPane tp) {
        if (c.isSelected()) {
            tp.setText("");
        }
    }

    public void createUIComponents() {
        // TODO: place custom component creation code here
    }
    
    public void getPackagesList(JComboBox list) throws IOException, InterruptedException {
        ProcessBuilder ps = new ProcessBuilder("adb","-s",selectedDevice,"shell","pm","list","packages","-3");
        ps.redirectErrorStream(true);
        cancelLogcat(prPack);
        prPack = ps.start();
        DefaultComboBoxModel comboMain = new DefaultComboBoxModel();
        new Thread() {
            @Override
            public void run() {
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(prPack.getInputStream()));
                    String line;
                    while ((line = in.readLine()) != null) {
                        line = line.replaceAll("package:","");
                        if (!"".equals(line)){comboMain.addElement(line);}
                        
                        System.out.print(line+",");
                    }
                    System.out.println("ok!");
                } catch (IOException ex) {
                    Logger.getLogger(MainLogicClass.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
        list.setModel(comboMain);
    }
    
    public void getDevicesList(JComboBox list) throws IOException, InterruptedException {
        ProcessBuilder ps = new ProcessBuilder("adb","devices");
        ps.redirectErrorStream(true);
        cancelLogcat(prDev);
        prDev = ps.start();
        DefaultComboBoxModel comboMain = new DefaultComboBoxModel();
        new Thread() {
            @Override
            public void run() {
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(prDev.getInputStream()));
                    String line;
                    while ((line = in.readLine()) != null) {
                        if (line.contains("devices")!=true){
                            line = line.replaceAll("\\s+",""); 
                            line = line.replace("device", "");
                            if (!"".equals(line)){comboMain.addElement(line);}
                            System.out.print(line+",");}
                    }
                    System.out.println("ok!");
                } catch (IOException ex) {
                    Logger.getLogger(MainLogicClass.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
        list.setModel(comboMain);
    }

    private void startOutputTimer(JTextArea ta) {
        bufferMainTimerTask = new TimerTask() {
            @Override
            public void run() {
                if (mainOutputBufferString!=null)
                {
                    ta.append(mainOutputBufferString.toString());
                    mainOutputBufferString.setLength(0);
                }
                /*else
                {
                    System.out.println("Nothing here!");
                }*/
            }
        };
        
        bufferMainTimer.schedule(bufferMainTimerTask, 100, 10);
    }
    
    //Functions to use with JTextPane. Some variables are placed here too.
    public void appendToJTPane(String str, JTextPane tp) throws BadLocationException
    {        
        StyledDocument document = (StyledDocument) tp.getDocument();
        document.insertString(document.getLength(), str, null);
    }
    
    private void startOutputToPaneTimer(JTextPane tp) {
        bufferMainTimerTask = new TimerTask() {
            @Override
            public void run() {
                if (mainOutputBufferString!=null)
                {
                    try {
                        appendToJTPane(mainOutputBufferString.toString(),tp);
                        mainOutputBufferString.setLength(0);
                    } catch (BadLocationException ex) {
                        Logger.getLogger(MainLogicClass.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        };
        
        bufferMainTimer.schedule(bufferMainTimerTask, 100, 10);
    }
    
    }

